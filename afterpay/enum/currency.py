from enum import Enum

class Currency(Enum):
    AUD = "AUD"
    USD = "USD"
    NZD = "NZD"
    CAD = "CAD"
    # clearpay
    GBP = "GBP"