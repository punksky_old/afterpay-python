Region = {
    "EU_SandBox": {
        "EndPoint": "https://api.eu-sandbox.afterpay.com"
    },
    "EU": {
        "EndPoint": "https://api.eu.afterpay.com"
    },
    "AU/NZ_SandBox": {
        "EndPoint": "https://api.sandbox.afterpay.com"
    },
    "AU/NZ": {
        "EndPoint": "https://api.afterpay.com"
    },
    "US/CA_SandBox": {
        "EndPoint": "https://api.us-sandbox.afterpay.com"
    },
    "US/CA": {
        "EndPoint": "https://api.us.afterpay.com"
    },
}