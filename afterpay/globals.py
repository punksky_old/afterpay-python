from platform import python_version, python_implementation
import sys
Version = '0.2.0'
UserAgent = 'AfterPayLibrary/{0} ({1}/{2}; {3})'.format(
            Version, 
            python_implementation(), 
            python_version(), 
            sys.platform
            )