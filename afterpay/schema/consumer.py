Consumer = {
    "email": {'required': True, 'type': 'string', 'maxlength': 128},
    "givenNames": {'required': False, 'type': 'string', 'maxlength': 128},
    "surname": {'required': False, 'type': 'string', 'maxlength': 128}, 
    "phoneNumber": {'required': False, 'type': 'string', 'maxlength': 32}
}