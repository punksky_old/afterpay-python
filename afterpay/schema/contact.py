Contact = {
    "name": {'required': True, 'type': 'string', 'maxlength': 255},
    "line1": {'required': True, 'type': 'string', 'maxlength': 128},
    "line2": {'required': False, 'type': 'string', 'maxlength': 128},
    "area1": {'required': False, 'type': 'string', 'maxlength': 128},
    "area2": {'required': False, 'type': 'string', 'maxlength': 128},
    "region": {'required': False, 'type': 'string', 'maxlength': 128},
    "postcode": {'required': True, 'type': 'string', 'maxlength': 128},
    "countryCode": {'required': True, 'type': 'string', 'maxlength': 2},
    "phoneNumber": {'required': False, 'type': 'string', 'maxlength': 32}
}