from .money import Money

Discount = {
    "displayName": {'required': True, 'type': 'string', 'maxlength': 128},
    "amount": Money
}