from . import Consumer, Contact, Item, Discount, Money

OrderDetails = {
    "consumer": Consumer,
    "billing": Contact,
    "shipping": Contact,
    "courier": None,
    "items": { "type": "list", 'schema': Item },
    "discounts": { "type": "list", 'schema': Discount },
    "taxAmount": Money,
    "shippingAmount": Money
    
}