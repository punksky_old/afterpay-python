Money = {
    "name": {'required': True, 'type': 'float'},
    "currency": {'required': True, 'type': 'string', 'maxlength': 3}
}