from .money import Money

Item = {
    "name": {'required': True, 'type': 'string', 'maxlength': 255},
    "sku": {'required': False, 'type': 'string', 'maxlength': 128},
    "quantity": {'required': True, 'type': 'number'},
    "pageUrl": {'required': False, 'type': 'string', 'maxlength': 2048},
    "imageUrl": {'required': False, 'type': 'string', 'maxlength': 2048},
    "price": Money,
    "categories": {'required': False, 'type': 'list'},
}