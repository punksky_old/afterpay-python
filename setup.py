import setuptools

from afterpay.globals import Version

with open("README.md", 'r') as f:
    long_description = f.read()


setuptools.setup(
    name="afterpay",
    version=Version,
    author="PunkSky",
    author_email="gitlab@punksky.dev",
    descritpion="afterpay ",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/punksky/afterpay-python",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent"
    ],
    packages=setuptools.find_packages(),
    install_requires=[
        "requests",
        "cerberus"
    ],
    zip_safe=False,
    include_package_data=True
)